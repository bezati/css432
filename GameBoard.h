#pragma once
#include <string>
#include <iostream>
class GameBoard
{
public:
	GameBoard(int playerTurn);     // Constructor
	bool addToken(int columnNum);  // Adds token to game board
	void printBoardStatus();       // Prints current state of board
	int getWinner();               // Returns playerNumber of winner or -1 if no one has won
	int getPlayerTurn();           // Returns playerNum for player whose turn it is
	bool isDraw();                 // Checks if gameboard is full
	~GameBoard();                  // Destructor


private:
	// attributes
	char ** board;
	int playerTurn;
	int winner;

	// methods
	bool checkWin(int columnNum, int rowNum);      // Checks to see if last token placed in column resulted in a win
	bool checkVertical(int columnNum);             // Helper for checkWin
	bool checkHorizontal(int rowNum);              // Helper for checkWin
	bool checkDiagonal(int rowNum, int columnNum); // Helper for checkWin
	int getRow(int columnNum);                     // Returns row for top token in the given column
	void togglePlayer();                           // Changes playerTurn to next player

};

