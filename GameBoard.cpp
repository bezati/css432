#include "GameBoard.h"


// Constructor: Dynamically allocate the gameboard and initialize player turn to player number in parameter
GameBoard::GameBoard(int playerTurn)
{
	// Winner is -1 until a player wins the game
	this->winner = -1;
	// If invalid playerTurn, default to 1
	if (playerTurn < 1 || playerTurn > 2) {
		playerTurn = 1;
	}
	else {
		this->playerTurn = playerTurn;
	}
	// Initialize gameboard values to '_'
	board = new char*[7];
	for (int i = 0; i < 7; i++) {
		board[i] = new char[6];
		for (int j = 0; j < 6; j++) {
			board[i][j] = '_';
		}
	}
}

// Adds a token to the gameboard. Returns true if successful
// Token [Row] [Column]
bool GameBoard::addToken(int columnNum) {

	// Check if column is full or out of bounds
	if (columnNum >= 7 || columnNum < 0 ||board[columnNum][5] != '_') {
		return false;
	}

	for (int i = 0; i < 6; i++) {
		if (board[columnNum][i] == '_') {
			board[columnNum][i] = '0' + playerTurn;
			checkWin(columnNum, getRow(columnNum));
			togglePlayer();
			return true;
		}
	}
}

// Prints the current gameboard
void GameBoard::printBoardStatus() {
	std::cout << "           ";
	for (int i = 5; i >= 0; i--) {
		for (int j = 0; j < 7; j++) {
			std::cout << board[j][i] << "  ";
		}
		std::cout << std::endl<<"           ";
	}
	std::cout << "-------------------" << std::endl;
	std::cout << "Column #   0  1  2  3  4  5  6" << std::endl;
	return;
}

// returns playerNumber of winner or -1 if there isn't one
int GameBoard::getWinner() {
	return winner;
}

// Returns playerNum for player whose turn it is
int GameBoard::getPlayerTurn() {
	return playerTurn;
}

// Checks to see if board is full. Returns true if it is or false if it is not
bool GameBoard::isDraw() {
	for (int i = 0; i < 7; i++) {
		if (board[i][5] == '_') {
			return false;
		}
	}
	return true;
}

// Destructor: Deallocate board from memory
GameBoard::~GameBoard()
{
	for (int i = 0; i < 7; i++) {
		delete[] board[i];
	}
	delete[] board;
}

// Checks if token added resulted in a win. Returns true if it did and sets winner to playerNum of winner
bool GameBoard::checkWin(int columnNum, int rowNum) {
	if (checkHorizontal(rowNum) || checkVertical(columnNum) || checkDiagonal(rowNum, columnNum)) {
		winner = playerTurn;
		return true;
	}
	return false;
}

// Checks if token resulted in a horizontal four-in-a-row
bool GameBoard::checkHorizontal(int rowNum) {
	int counter = 0;              // counter to keep track of tokens in a row
	for (int i = 0; i < 7; i++) {
		if (counter == 4) {
			return true;
		}
		else if (board[i][rowNum] == '0' + playerTurn) {
			counter++;
		}
		else {
			counter = 0;
		}
	}
	if (counter == 4) {
		return true;
	}
	return false;
}

// Checks if token resulted in a vertical four-in-a-row
bool GameBoard::checkVertical(int columnNum) {
	int counter = 0;              // counter to keep track of tokens in column
	for (int i = 0; i < 6; i++) {
		if (counter == 4) {
			return true;
		}
		else if (board[columnNum][i] == '0' + playerTurn) {
			counter++;
		}
		else {
			counter = 0;
		}
	}
	if (counter == 4) {
		return true;
	}
	return false;
}

// Checks if token resulted in a diagonal four-in-a-row
bool GameBoard::checkDiagonal(int rowNum, int columnNum) {
	int counter = 0;
	// Check diagonal up (left to right)
	int baseRow = rowNum;
	int baseColumn = columnNum;
	while (baseRow != 0 && baseColumn != 0) {
		baseRow--;
		baseColumn--;

	}
	while (baseColumn < 7 && baseRow < 6) {
		if (counter == 4) {
			return true;
		}
		else if (board[baseColumn++][baseRow++] == '0' + playerTurn) {
			counter++;
		}
		else {
			counter = 0;
		}
	}
	if (counter == 4) {
		return true;
	}
	
	// Check diagonal down (left to right)
	counter = 0;
	baseRow = rowNum;
	baseColumn = columnNum;
	while (baseRow != 6 && baseColumn != 0) {
		baseRow++;
		baseColumn--;
	}
	while (baseColumn < 7 && baseRow >= 0) {
		if (counter == 4) {
			return true;
		}
		else if (board[baseColumn++][baseRow--] == '0' + playerTurn) {
			counter++;
		}
		else {
			counter = 0;
		}
	}
	if (counter == 4) {
		return true;
	}
	return false;
}

// Returns rowNum of token at the top of the column. Returns -1 for error.
int GameBoard::getRow(int columnNum) {
	// Return -1 if the column is empty
	if (board[columnNum][0] == '_') {
		return -1;
	}
	// Find top token in column
	for (int i = 0; i < 6; i++) {
		if (i == 6) {
			return i;
		}
		if (board[columnNum][i + 1] == '_') {
			return i;
		}
	}
}

// Changes playerTurn. Only should be called in addToken. Must call AFTER checkWin
void GameBoard::togglePlayer() {
	if (playerTurn == 1) {
		playerTurn = 2;
	}
	else {
		playerTurn = 1;
	}
}
