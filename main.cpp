#include <iostream>
#include "GameBoard.h"
using namespace std;

int main() {
	int player;
	int column;
	std::cout << "Enter the number of the player who will go first (1 or 2) : ";
	std::cin >> player;
	GameBoard gameboard(player);
	gameboard.printBoardStatus();

	while (gameboard.getWinner() == -1 && !gameboard.isDraw()) {
		std::cout << "\nPlayer " << gameboard.getPlayerTurn() << "'s turn\nEnter the column to put token in (0-6): ";
		std::cin >> column;
		if (!gameboard.addToken(column)) {
			std::cout << "\nError: invalid column. Please try again";
		}
		else {
			gameboard.printBoardStatus();
		}
	}
	if (gameboard.isDraw()) {
		std::cout << "\nIt is a draw" << std::endl;
	}
	else {
		std::cout << "\nPlayer " << gameboard.getWinner() << " wins!!" << std::endl;
	}
	int x;
	cin >> x;
	return 0;
}